# SQL Basics


This is a repo to introduce SQL and DBs.


Objectives: 
- Intro to SQL and databases
- Install mysql and workbench
- Define a table and columns


## Introduction

A database can be a file containing lots of information. It could be a recipe book, excel spreadsheet with contacts or a bunch of post-its with people's name and email address.

These can be considered databases but really they are not very organised or easy to use. They have limitations. In tech, the following wouold be what people are referring to as DBs.

- SQL
- noSQL (not only SQL)


**SQL?* - Structure Query Language

How is it structured? It has structure tables with connection between tables. Imagine excel sheets connected to one another. This makes it easy to get info we want.


#### Common terminology

**DB**- Database
**SQL**- Structure Query Language- how a DB might be organised
**DBMS**- DataBase Management System

Inside an SQL DB, you will have:
- Columns
- Rows
- Tables
  

| title                  | author_id | date | price | genre     |
|------------------------|-----------|------|-------|-----------|
| Harry Potter           | 1         | 2001 | 12    | fiction   |
| Harry Potter 2         | 1         | 2003 | 12    | fiction   |
| Good Vibes - Good life | 2         | 2020 | 20    | Self Help |



| First_name | Last_name | email              |   |   |
|------------|-----------|--------------------|---|---|
| J. K.      | Rowling   | jk@gmail.com       |   |   |
| Harry      | Bookster  | hb@bookster.com    |   |   |
| Vex        | King      | king@kingbooks.com |   |   |



| type      | description                                 | other_info |   |   |
|-----------|---------------------------------------------|------------|---|---|
| fiction   | these are fictional noval                   | none       |   |   |
| Biography | some what true accounts of people's lives   | none       |   |   |
| Selfhelp  | Meant to help you think and better yourself | none       |   |   |



We can see connections between the above tables. However, machines cannot and so we need **Primary Keys** and **Foreign Keys**.

Every table has to have a non-empty column which serves as the primary key. Primary keys are unique and cannot be reused (at least in good SQL DBs).

Foreign keys reference a primary key. They are essential to the primary keys in the secondary table.

Let's see the above example with primary keys:

| book_id  | title                  | author_id | date | price | genre     |
|----------|------------------------|-----------|------|-------|-----------|
| 1        | Harry Potter           | 1         | 2001 | 12    | fiction   |
| 2        | Harry Potter 2         | 1         | 2003 | 12    | fiction   |
| 3        | Good Vibes - Good life | 3         | 2020 | 20    | Self Help |



Authors

| author_id | First_name | Last_name | email              |   |   |
|-----------|------------|-----------|--------------------|---|---|
| 1         | J. K.      | Rowling   | jk@gmail.com       |   |   |
| 2         | Harry      | Bookster  | hb@bookster.com    |   |   |
| 3         | Vex        | King      | king@kingbooks.com |   |   |


### The keys create relationships, cute. There's one to one (1:1), one to many (1:N) and many to many (N:N)

You can then make entity diagrams displaying the relationships.


### SQL Data Types

There are different types. Charindex (character index), Charmax, decimals and integers. 


### MySQL & Maria DB

Once you have installed, enable and started mysql or mariadb

```bash
mysql -u root -p -h localhost
```



# DDL- Data Definition Lnaguage

Creating tables and columns

```sql
-- Create a my_db
CREATE DATABASE my_db;

-- Create a table
CREATE TABLE movie_table (
    film_name VARCHAR(60);
    releaste_date DATE;
    description VARCHAR(120)
);
```

In SQL, we need to define the Datatypes for collumns. The following exist:

- VARCHAR(somenumber)     -adaptable to different character lengths
- VARCHAR(max)            -same as above w max length
- CHARACTER or CHAR       -always takes exact size
- Int                     -integers i.e. whole numbers
- DATE, TIME, DATETIME    
- DECIMALS or NUMERIC     -defines the number of digits to the right of the    decimal
- BINARY                  -used to store binary info such as pictures or music
- FLOAT                   -for very large numbers
- BIT                     -similar to binary (0, 1, NULL)






##### Creating Tables

```sql
-- creating the film table
CREATE TABLE film_table ( filmID int NOT NULL, film_name VARCHAR(60), release_data DATE, description VARCHAR(200), PRIMARY KEY (filmIK) )


 desc film_table;
-- creating the director table
CREATE TABLE director_table ( 
    direcotID int NOT NULL, first_name VARCHAR(60), last_name VARCHAR(60) NOT NULL, email VARCHAR(120), PRIMARY KEY (directorIK))

```

##### Altering Tables


```SQL

-- add nationality to director table

-- add direcotID as a FOREIGN KEY in film_table

-- Alter films table so that title cannot be null

Alter table film_table MODIFY COLUMN film_name NOT NULL;
-- or
ALTER TABLE film_table CHANGE film_name film_name VARCHAR(60) NOT NULL;

-- Alter directors table to add a description to each director 

ALTER TABLE director_table ADD COLUMN description VARCHAR(200)

-- Alter table to remove description to each director

ALTER TABLE DROP COLUMN 

-- What is a dependent destroy constraint and how is it useful? 

```



### Data Manipulation Languae

Inserting, deling and altering row.

- INSERT INTO <TABLE> (dirctot_ID, First_name) VALUES ( 10 , 'Shannon')
- INSERT  <TABLE> VALUES ( 10 , 'Shannon', 'Lucas', 'sl@lucasfilms.com')
- DELETE FROM <table> WHERE COLUMN = <ID>
- UPDATE FROM <table> WHERE COLUMN = <ID>


### Data Query Language

Play around with SQL queering in the w3schools sql section.

```sql

-- normal Selector clause and From clause
SELECT <Column> FROM <Table>;

-- normal Selector clause and From clause + Where clause
SELECT <Column> FROM <Table> WHERE id=id;

-- Operators
SELECT <Column> FROM <Table> WHERE price = x
SELECT <Column> FROM <Table> WHERE price != x -- not equal
SELECT <Column> FROM <Table> WHERE price < x
SELECT <Column> FROM <Table> WHERE price > x
SELECT <Column> FROM <Table> WHERE price <= x
SELECT <Column> FROM <Table> WHERE price >= x

-- Logical and / or in Select 

SELECT <Column> FROM <Table> WHERE price = x AND categoryID =1 -- Both sides need to be true to get said value
SELECT <Column> FROM <Table> WHERE price = x or categoryID =1 -- Only one side need to be true to get value

-- Matcher & Wild cards %_[charlist][^charlist] LIKE
-- % is the wild card for any number or characters and any character 
SELECT <Column> FROM <Table> WHERE name LIKE 'j%';
SELECT <Column> FROM <Table> WHERE name LIKE '%SS';

-- _ is the substitute for one single character 
SELECT <Column> FROM <Table> WHERE name LIKE '____';

-- [charlist]% match 

-- [^charlist]% don't match


-- WHERE BETWEEN
SELECT * FROM [Products] WHERE PRICE BETWEEN 10 AND 22;

-- in ('list', 'items')
SELECT * FROM [Customers] WHERE country IN ('Germany', 'Mexico')


-- Alias - for columns and agregate data - Does not change original data
SELECT CustomerID AS ID, CustomerName AS 'Company Name', ContactName AS 'HUMAN TO INFLUENCE' FROM [Customers] WHERE country IN ('Germany', 'Mexico')

-- Alias the creation of new data
-- Figuring out how much to pay for 30 new of each product in category 1
SELECT *, price * 30 AS '$ to Order' FROM [Products] where categoryID = 1


-- Ordering our results
SELECT *, price * 30 AS '$ to Order' FROM [Products] where categoryID = 1 ORDER BY SupplierID DESC;

SELECT *, price * 30 AS '$ to Order' FROM [Products] where categoryID = 1 ORDER BY SupplierID ASC;

SELECT MIN(price) as 'Mininum price', MAX(price) as 'Max', AVG(price), SUM(price), COUNT(price) FROM [Products]

-- Aggregate functions with group by
SELECT SupplierID, MIN(price) as 'Mininum price', MAX(price) as 'Max', AVG(price), SUM(price), COUNT(price) FROM [Products] GROUP BY SupplierID


-- Aggregate functions whith group by and HAVING
SELECT SupplierID, AVG(price), COUNT(price) FROM [Products] GROUP BY SupplierID HAVING PRICE > 10


```