stuff missing





## Virtual environments in python venve

.venv folder is a virtual environtment for python, where you can install packages with pip locally per project, rather than globally in your machine.

You need to start a venv environtment and activate it, using 'source'.


code

```bash

python3 -m venv .venv
source .venv/bin/activate

```

This is to avoid package collision on your machine and contains the python packages installed to this folder.


**requirements.txt** is a text file where you can specify a list of packages your code will need. You can then load/read it using pip. It will also lock in versions.

Read it with pip or pip3 using this codeL

```bash
